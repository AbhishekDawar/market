﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Market.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
               name: "AdminProducts",
               url: "Admin/Product",
               defaults: new { controller = "Product", action = "Index" }
           );


            routes.MapRoute(
               name: "AllCategories",
               url: "categories/all",
               defaults: new { controller = "Category", action = "CategoryTable" }
           );
            routes.MapRoute(
            name: "EditCategory",
            url: "categories/Update",
            defaults: new { controller = "Category", action = "Edit" }
        );
            routes.MapRoute(
           name: "DeleteCategory",
           url: "categories/Remove",
           defaults: new { controller = "Category", action = "Delete" }
       );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
