﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class AdminViewModel
    {
        public int CategoryCount { get; set; }
        public int ProductCount { get; set; }
        public int ToalordersCount { get; set; }
        public int ConfigurationCount { get; set; }
    }
}