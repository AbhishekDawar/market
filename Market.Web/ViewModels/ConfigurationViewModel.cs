﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class ConfigurationViewModel
    {
        public Config SingleConfiguration { get; set; }
    }

    public class ConfigurationSearchViewModel
    {
        public List<Config> ConfigurationList { get; set; }
    }

    public class ConfigurationEditViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
