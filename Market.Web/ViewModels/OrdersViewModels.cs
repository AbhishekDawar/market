﻿using Market.Entities;
using Market.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class OrderViewModels
    {

        public List<Order> Orders { get; set; }
        public string userID { get; set; }
        public Pager Pager { get;  set; }
        public object status { get;  set; }
    }
    public class OrderDetailsViewModels
    {

        public Order Order { get; set; }
        public MarketUser OrderedBy { get; set; }
        public List<string> AvilableStatuses { get;  set; }
    }
}