﻿using Market.Entities;
using Market.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class ShopViewModel
    {
        public List<Product> Products { get; set; }
        public List<Category> FeaturedCategories { get; set; }
        public int MinimumPrice { get; set; }
        public int MaximumPrice { get; set; }
        public int? soryby { get;  set; }
        public int? CategoryID { get; set; }


        public Pager Pager { get; set; }
        public string SearchTerm { get;  set; }
    }
    public class CheckoutViewModel
    {
        public List<Product> CartProducts { get; set; }
        public List<int> CartProductIDs { get; set; }
        public MarketUser User { get; set; }
    }
    public class FilterProductViewModel
    {
        public List<Product> Products { get; set; }
         
        public Pager Pager { get; set; }
        public int? CategoryID { get;  set; }
        public int? soryby { get;  set; }
        public object SearchTerm { get;  set; }
    }
}