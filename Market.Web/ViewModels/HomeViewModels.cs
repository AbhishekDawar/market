﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{  
    public class HomeViewModels
    {
        public List<Category> FeaturedCategories { get; set; }
        public List<Product> products { get; set; }
    }
}