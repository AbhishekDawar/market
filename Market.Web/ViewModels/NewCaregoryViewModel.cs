﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class NewCaregoryViewModel
    {
        [Required]
        [MaxLength(60), MinLength(5)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public bool isFeatured { get; set; }

        public int CategoryID { get; set; }

        public string ImageUrl { get;  set; }
    }

    public class CategorySearchViewMOdel
    {
        public List<Category> Categories { get; set; }
        public string SearchTerm { get; set; }

        public Pager Pager { get; set; }
    }
    public class EditCategoryViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageURL { get; set; }

        public bool isFeatured { get; set; }
    }
} 