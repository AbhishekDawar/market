﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Market.Web.ViewModels
{
    public class ProductWidgetViewMOdel
    {
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }
        public bool IsLatestProducts { get; set; }
        public int CategoryIDForRelatedproduct { get; set; }
    }
}