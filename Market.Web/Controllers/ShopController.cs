﻿
using Market.Entities;
using Market.Services;
using Market.Web.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace Market.Web.Controllers
{

    public class ShopController : Controller
    {
        // GET: Shop







        private MarketSignInManager _signInManager;
        private MarketUserManager _userManager;


        public MarketSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<MarketSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public MarketUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<MarketUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }








     
        public ActionResult Index(string searchTerm, int? minimumPrice, int? maximumPrice, int? CategoryID, int? sortBy, int? pageNo)
        {
            ShopViewModel model = new ShopViewModel();


            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            model.soryby = sortBy;
            model.CategoryID = CategoryID;
            model.SearchTerm = searchTerm;



            model.FeaturedCategories = CategorysServices.Instance.GetFeaturedCategories();

            model.MaximumPrice = ProductsService.Instance.GetMaximumPrice();

            model.Products = ProductsService.Instance.SearchProducts(searchTerm, minimumPrice, maximumPrice, CategoryID, sortBy, pageNo.Value, 9);



            int totalCount = ProductsService.Instance.SearchProductsCount(searchTerm, minimumPrice, maximumPrice, CategoryID, sortBy);
            model.Pager = new Pager(totalCount, pageNo);

            return View(model);
        }

        [OutputCache(Duration = 60)]
         public ActionResult FilterProducts(string searchTerm, int? minimumPrice, int? maximumPrice, int? CategoryID, int? sortBy, int? pageNo)
        {
            FilterProductViewModel model = new FilterProductViewModel();

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            model.soryby = sortBy;
            model.CategoryID = CategoryID;

            model.SearchTerm = searchTerm;

            int totalCount = ProductsService.Instance.SearchProductsCount(searchTerm, minimumPrice, maximumPrice, CategoryID, sortBy);
            model.Products = ProductsService.Instance.SearchProducts(searchTerm, minimumPrice, maximumPrice, CategoryID, sortBy, pageNo.Value, 9);

            model.Pager = new Pager(totalCount, pageNo);

            return PartialView(model);
        }













        [Authorize]
        public ActionResult Checkout() // taking caart value from cookies and providing Amount
        {
            CheckoutViewModel model = new CheckoutViewModel();

            var cartProductsCookie = Request.Cookies["cartProducts"];
            //ProductsService productsService = new ProductsService();
            if (cartProductsCookie != null && !string.IsNullOrEmpty(cartProductsCookie.Value))
            {
                //var productIDs = cartProductsCookie.Value;
                //var ids = productIDs.Split('-');
                //List<int> pIds = ids.Select(x => int.Parse(x)).ToList();]
                //Above 3  line in single line 

                model.CartProductIDs = cartProductsCookie.Value.Split('-').Select(x => int.Parse(x)).ToList();

                model.CartProducts = ProductsService.Instance.GetProducts(model.CartProductIDs);

                model.User = UserManager.FindById(User.Identity.GetUserId());

            }

            return View(model);
        }

        public JsonResult PlaceOrder(string productIDs)
        {

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
             

            if (!string.IsNullOrEmpty(productIDs))
            {

                var productsQuantities = productIDs.Split('-').Select(x => int.Parse(x)).ToList();



                //var PIDs = productIDs.Split(new char[] { '-' }).Select(x => int.Parse(x)).Distinct().ToList();

                var BoughtProducts = ProductsService.Instance.GetProducts(productsQuantities.Distinct().ToList());   //here we get Bought Product Ids





                Order newOrder = new Order();
                newOrder.UserID = User.Identity.GetUserId();
                newOrder.OrdererAt = DateTime.Now;
                newOrder.Status = "Pending";

                //Total amount nikalne ke liye hamko ... product ki price and ouski Quantities ka Product krna hoga 
                //Product ki price Bought Products  me product ki id ke base pr ouska price lehenge
                //and quantity nikalne ke liye ham.. productsQuantities jo productIDs hai ounka ek ek krke Count nikalhenge
                // and me Quantity and Product ka multiply kr dehenge  

                newOrder.TotalAmount = BoughtProducts  //jo product bought kiye hai
                                        .Sum(product => product.Price   //ouske price ka Sum pick kro
                                        * productsQuantities            //Lekin Product ki Quantities 
                                        .Where(productID => productID == product.ID) // jo productQuantity me product Id hai ousko BoughtProduct Id se compare kro
                                        .Count());                     //Compare krne me bad.. id ka count nikalo.. nd ousko product Quantity Banalo


                newOrder.OrderItems = new List<OrderItem>();
                newOrder.OrderItems.AddRange(BoughtProducts.Select(x => new OrderItem() { ProductID = x.ID ,Quantity =productsQuantities.Where(productID => productID ==x.ID).Count() }));

                var rowsEffected = ShopService.Instance.SaveOrder(newOrder);

                result.Data = new { Success = true, Rows = rowsEffected };
            }
            else
            {
                result.Data = new { Success = false };
            }

            return result;
        }
    }
}