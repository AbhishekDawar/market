﻿using Market.Web.ViewModels;
using Market.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Market.Web.Controllers
{
    public class WidgetsController : Controller
    {
        // GET: Widgets
        [OutputCache(Duration = 60)]
        public ActionResult Products(bool isLatestProducts, int? CategoryID = 0)
        {

            ProductWidgetViewMOdel model = new ProductWidgetViewMOdel();
            model.CategoryIDForRelatedproduct = CategoryID.Value;
            model.IsLatestProducts = isLatestProducts;
            if (isLatestProducts)
            {
                model.Products = ProductsService.Instance.GetLatestProducts(4);
                // Taking 4 latest product for Home page latest Product Section
            }
            else if (CategoryID.HasValue && CategoryID.Value > 0 && isLatestProducts == false)
            {
                ViewBag.id = 0;
                model.Products = ProductsService.Instance.GetProductsByCategory(CategoryID.Value, 4);
                // 
            }
            else
            {
                model.Categories = CategorysServices.Instance.GetFeaturedCategories();
                //Home Page OurPage section Category wise Sorting
                model.Products = ProductsService.Instance.GetProducts(1, 8);
                // Taking 8 latest product for Home page Our Product Section
            }
            return PartialView(model);
        }

    }
}