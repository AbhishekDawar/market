﻿using Market.Services;
using Market.Web.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class OrderController : Controller
    {

        private MarketSignInManager _signInManager;
        private MarketUserManager _userManager;


        public MarketSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<MarketSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public MarketUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<MarketUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        public ActionResult Index()
        {
            return PartialView("_Index");
        }


        // GET: Order
        public ActionResult OrderTable(string userID, string status, int? pageNo)
        {
            OrderViewModels model = new OrderViewModels();
            int pageSize = ConfigurationService.Instance.PagesizeConfiguration();
            model.userID = userID;
            model.status = status;
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;

            model.Orders = OrdersService.Instance.SearchOrders(userID, status, pageNo.Value, pageSize);
            var totalRecord = OrdersService.Instance.SearchOrdersCount(userID, status);

            if (model.Orders != null)
            {

                model.Pager = new Pager(totalRecord, pageNo, pageSize);
                return PartialView("_OrderList", model);
            }
            else
            {
                return HttpNotFound();
            }
        }



        public ActionResult Details(int ID)
        {
            OrderDetailsViewModels model = new OrderDetailsViewModels();


            model.Order = OrdersService.Instance.GetOrderById(ID);
            if (model.Order != null)
            {
                model.OrderedBy = UserManager.FindById(model.Order.UserID);

            }
            model.AvilableStatuses = new List<string>() { "Pending", "In Progress", "Develered" };
            return PartialView("_Details", model);
        }

        public JsonResult ChangeStatus(string status, int ID)
        {

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            result.Data = new { Success = OrdersService.Instance.UpdateOrderStatus(ID,status) };

            return result;

        }
    }
}