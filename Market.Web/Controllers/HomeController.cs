﻿using Market.Services;
using Market.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class HomeController : Controller
    {
        //  CategorysServices categorysServices = new CategorysServices();

        public ActionResult Index()
        {
            HomeViewModels model = new HomeViewModels();
            model.FeaturedCategories = CategorysServices.Instance.GetFeaturedCategories();
            //model.products = 
            
            return View(model);
        }

    }
}