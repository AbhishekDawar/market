﻿using Market.Entities;
using Market.Services;
using Market.Web.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class ProductController : Controller
    {

        //ProductsService productsService = new ProductsService();
       // CategorysServices CategorysServices = new CategorysServices();

        public ActionResult Index()
        {

            return PartialView();
        }

        public ActionResult ProductTable(string search, int? pageNo)
        {

            ProductSearchViewModel model = new ProductSearchViewModel();

            model.SearchTerm = search;
            pageNo = pageNo.HasValue ? pageNo.Value>0 ? pageNo.Value : 1 :1;
            //            simmilar to above single line
            //            if (pageNo.HasValue)
            //            { if(pageNo.Value>0)
            //                {
            //                    model.pageNo = pageNo.Value;
            //                }
            //                else
            //                {
            //                    model.pageNo = 1
            //;                }
            //            }
            //            else { model.pageNo = 1; }

            // model.Products = ProductsService.Instance.GetProducts(model.pageNo);

            var totalRecord = ProductsService.Instance.GetProductsCount(search);
            model.Products = ProductsService.Instance.GetProducts(search, pageNo.Value);

            if (model.Products != null)
            {
                int pageSize = ConfigurationService.Instance.PagesizeConfiguration();
                model.Pager = new Pager(totalRecord, pageNo, pageSize);
                return PartialView("ProductTable", model);
            }
            else
            {
                return HttpNotFound();
            }

            //   products = products.Where(p => p.Name == search).ToList();
            //if (!string.IsNullOrEmpty(search))
            //{
            //    model.SearchTerm = search;
            //   model.Products = model.Products.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            //}
            // return PartialView(model);
        }

        //Create 
        [HttpGet]
        public ActionResult Create()
        {
            NewProductViewModel model = new NewProductViewModel();
            model.AvailableCategories= CategorysServices.Instance.GetAllCategories();

            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Create(NewProductViewModel model) // Creating new Product
        {
            if (ModelState.IsValid)
            {
                var newProduct = new Product()
                {
                    Name = model.Name,
                    Description = model.Description,
                    Price = model.Price,
                    ImageUrl = model.ImageURL,
                    //CategoryID=model.CategoryID
                    Category = CategorysServices.Instance.GetCategory(model.CategoryID)
                };
                ProductsService.Instance.saveProduct(newProduct);
                return RedirectToAction("ProductTable");
            }
            else
            {
                return PartialView(model);
            }
        
        }
        //Edit
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditProductModel model = new EditProductModel();

            var product = ProductsService.Instance.GetProduct(ID);
            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.Price = product.Price;
            model.CategoryID = product.Category != null ? product.Category.ID : 0;
            model.ImageUrl = product.ImageUrl;

            model.AvailableCategories = CategorysServices.Instance.GetAllCategories();


            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(EditProductModel model) // taking value from view in Product object 
        {
            var existingProduct = ProductsService.Instance.GetProduct(model.ID);
            existingProduct.Name = model.Name;
            existingProduct.Description = model.Description;
            existingProduct.Price = model.Price;
         

            existingProduct.Category = null; //mark it null. Because the referncy key is changed below
            existingProduct.CategoryID = model.CategoryID;

            // dont update imageURL if its empty
            if (!string.IsNullOrEmpty(model.ImageUrl))
            {
                existingProduct.ImageUrl = model.ImageUrl;
            }

            //ProductsService.Instance.UpdateProduct(existingProduct);



            ProductsService.Instance.updateProduct(existingProduct); // now passing Product value to USER-DEFINED Function which is created in Market Product class. 
            return RedirectToAction("ProductTable");
        }
        [HttpPost]
        public ActionResult Delete(int ID) // taking value from view in Product object 
        {
            ProductsService.Instance.DeleteProduct(ID); // now passing Product value to USER-DEFINED Function which is created in Market Product class. 
            return RedirectToAction("ProductTable");
        }


        [HttpGet]
        public ActionResult ProductDetails(int ID )
        {
            ProductViewModel model = new ProductViewModel();

            model.Product = ProductsService.Instance.GetProduct(ID);

            return View(model);
        }

    }

}