﻿using Market.Services;
using Market.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            AdminViewModel model = new AdminViewModel();
            model.CategoryCount = CategorysServices.Instance.GetCategoriesCount("");
            model.ProductCount = ProductsService.Instance.GetProductsCount("");
            model.ConfigurationCount = ConfigurationService.Instance.GetConfigurationCount();
            model.ToalordersCount = OrdersService.Instance.SearchOrdersCount("", "");
            return View(model);
        }
        // GET: Admin
        public ActionResult Category()
        {
            return View();
        }
        public ActionResult Products()
        {
              return View();
        }
        public ActionResult Configurations()
        {
            return View();
        }
        public ActionResult Orders()
        {
            return View();
        }
    }
}