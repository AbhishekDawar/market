﻿using Market.Entities;
using Market.Services;
using Market.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class ConfigurationController : Controller
    {
      //  ConfigurationService ConfigurationService = new ConfigurationService();
      
        // GET: Configuration
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult ConfigurationTable()
        {

            ConfigurationSearchViewModel model = new ConfigurationSearchViewModel();
               model.ConfigurationList = ConfigurationService.Instance.GetConfigurations();

            return PartialView(model);

        }

        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(Config config)
        {
            ConfigurationService.Instance.SaveConfiguration(config);
            return RedirectToAction("ConfigurationTable");
        }

        [HttpGet]
        public ActionResult Edit(string ID)
        {
            ConfigurationEditViewModel model = new ConfigurationEditViewModel();
            var SingleConfiguration = ConfigurationService.Instance.GetConfiguration(ID);
            model.Key = SingleConfiguration.Key;
            model.Value = SingleConfiguration.Value;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(ConfigurationEditViewModel model)
        {
            var existingConfiguration = ConfigurationService.Instance.GetConfiguration(model.Key);
            existingConfiguration.Key = model.Key;
            existingConfiguration.Value = model.Value;

            ConfigurationService.Instance.UpdateConfiguration(existingConfiguration);

            return RedirectToAction("ConfigurationTable");
        }

        [HttpPost]
        public ActionResult Delete(string ID)
        {
            ConfigurationService.Instance.DeleteConfiguation(ID);
            return RedirectToAction("ConfigurationTable ");
        }


    }
}