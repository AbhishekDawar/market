﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Market.Services;
using Market.Web.ViewModels;
using System.Data.Entity;

namespace Market.Web.Controllers
{
    public class CategoryController : Controller
    {
        //CategorysServices categoryService = new CategorysServices(); // because we have Market Service class in which we are taking to Database
        #region Mainpage-Index-contain-CategoryTable-with-Action
        [HttpGet] //list of Category
        public ActionResult Index() // using for listing Caregory 
        {
         //   var categories = CategorysServices.Instance.GetAllCategories(); // Calling service layer User-define fu nction for taking category

            return PartialView("_Index"); //categories

        }
        #endregion

        #region category-table

        public ActionResult CategoryTable(string search, int? pageNO)
        {

            CategorySearchViewMOdel model = new CategorySearchViewMOdel();
            model.SearchTerm = search;

            pageNO = pageNO.HasValue ? pageNO.Value > 0 ? pageNO.Value : 1 : 1;

            var totalRecord = CategorysServices.Instance.GetCategoriesCount(search);

            model.Categories = CategorysServices.Instance.GetCategories(search, pageNO.Value);
            if (model.Categories != null)
            {
                int pageSize = int.Parse(ConfigurationService.Instance.GetConfiguration("ListingPageSize").Value);
                model.Pager = new Pager(totalRecord, pageNO, pageSize);
                return PartialView("_CategoryTable", model);
            }
            else
            {
                return HttpNotFound();
            }

        }
        #endregion

        #region Creation
        [HttpGet] // createing category
                public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken()]
        [HttpPost]
        public ActionResult Create(NewCaregoryViewModel model) // taking value from view in category object 
        {
            if (ModelState.IsValid)
            {
                var NewCategory = new Category();
                NewCategory.Name = model.Name;
                NewCategory.Description = model.Description;
                NewCategory.ImageUrl = model.ImageUrl;
                NewCategory.IsFeatured = model.isFeatured;

                CategorysServices.Instance.saveCategory(NewCategory); // now passing Category value to USER-DEFINED Function which is created in Market category class. 

                return RedirectToAction("CategoryTable");
            }
            else
            {
                return new HttpStatusCodeResult(500);
            }
         
        }
        #endregion

        #region Updation
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            EditCategoryViewModel model = new EditCategoryViewModel();
            var category = CategorysServices.Instance.GetCategory(Id);
            model.ID = category.ID;
            model.Name = category.Name;
            model.Description = category.Description;
            model.ImageURL = category.ImageUrl;
            model.isFeatured = category.IsFeatured;


            return PartialView("_Edit",model);
        }

        [HttpPost]
        public ActionResult Edit(EditCategoryViewModel model)
        {
            var existingCategory = CategorysServices.Instance.GetCategory(model.ID);
            existingCategory.Name = model.Name;
            existingCategory.Description = model.Description;
            existingCategory.ImageUrl = model.ImageURL;
            existingCategory.IsFeatured = model.isFeatured;

            CategorysServices.Instance.updateCategory(existingCategory);

            return RedirectToAction("CategoryTable");
        }
        #endregion

        #region Deletion
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            CategorysServices.Instance.DeleteCategory(ID);
            return RedirectToAction("CategoryTable");
        }
        #endregion
    }
}