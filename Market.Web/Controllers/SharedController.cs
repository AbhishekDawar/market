﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.Web.Controllers
{
    public class SharedController : Controller
    {
       //Image uploading 

        public JsonResult UploadImage()
        {
            JsonResult result = new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            try
            {
                var file = Request.Files[0];

                var filename =Guid.NewGuid() + Path.GetExtension(file.FileName);

                var path = Path.Combine(Server.MapPath("~/Content/Images/"), filename);

                file.SaveAs(path);

                result.Data = new { Success = true, ImageURL = string.Format("/Content/Images/{0}",filename) };

                //  var newImage = new Image() { name = filename };

               
            }
            catch(Exception ex)
            {
                result.Data = new { success = false , Message = ex.Message  };
            }

            return result;
                      
        }
    }
}