﻿using Market.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.DataBase
{
    public class MarketContext : IdentityDbContext<MarketUser>, IDisposable    //DbContext  //
    {
        public MarketContext() : base("name=MarketConnection")  // Passing Connection String Name base Constructor for connection establishment  
        {

        }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)    fluent Api which is not used in project right now
        //{
        //    modelBuilder.Entity<Category>().Property(p => p.Name).IsRequired().HasMaxLength(50);
        //}

        public DbSet<Category> Categories { get; set; }             // ye table bnao db me
        public DbSet<Product> Products { get; set; }                // ye table bnao db me
        public DbSet<Config> Configurations { get; set; }          // ye table bnao db me
        public DbSet<Order> Orders { get; set; }          // ye table bnao db me
        public DbSet<OrderItem> OrderItems { get; set; }          // ye table bnao db me



        public static MarketContext Create()
        {
            return new MarketContext();
        }
    }


}
