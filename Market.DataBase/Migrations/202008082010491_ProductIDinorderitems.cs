﻿namespace Market.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductIDinorderitems : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "Product_ID", "dbo.Products");
            DropIndex("dbo.OrderItems", new[] { "Product_ID" });
            RenameColumn(table: "dbo.OrderItems", name: "Product_ID", newName: "ProductID");
            AlterColumn("dbo.OrderItems", "ProductID", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderItems", "ProductID");
            AddForeignKey("dbo.OrderItems", "ProductID", "dbo.Products", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderItems", "ProductID", "dbo.Products");
            DropIndex("dbo.OrderItems", new[] { "ProductID" });
            AlterColumn("dbo.OrderItems", "ProductID", c => c.Int());
            RenameColumn(table: "dbo.OrderItems", name: "ProductID", newName: "Product_ID");
            CreateIndex("dbo.OrderItems", "Product_ID");
            AddForeignKey("dbo.OrderItems", "Product_ID", "dbo.Products", "ID");
        }
    }
}
