﻿namespace Market.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class validationerror : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 60));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
