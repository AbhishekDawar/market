﻿using Market.DataBase;
using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Market.Services
{
    public class OrdersService
    {

        #region Singleton

        private static OrdersService instance { get; set; }
        public static OrdersService Instance
        {
            get
            {
                if (instance == null) instance = new OrdersService();
                return instance;
            }
        }

        private OrdersService()
        {
        }
        #endregion




        public List<Order> SearchOrders(string userID, string status, int pageNo, int pageSize)
        {
            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {
                var orders = context.Orders.ToList();  // Products ki List Li  hai Database se 

                if (!string.IsNullOrEmpty(userID))  // Ager Search Term null nahi hai to 
                {
                    orders = orders.Where(x => x.UserID.ToLower().Contains(userID.ToLower())).ToList(); // vo product lelo jiska name product contain krta ho
                }

                if (!string.IsNullOrEmpty(status))  // Ager Search Term null nahi hai to 
                {
                    orders = orders.Where(x => x.Status.ToLower().Contains(status.ToLower())).ToList(); // vo product lelo jiska name product contain krta ho
                }

                return orders.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList(); // ab products ko return krdo pagination lga kr 

            }
        }

        public int SearchOrdersCount(string userID, string status)
        {
            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {
                var orders = context.Orders.ToList();  // Products ki List Li  hai Database se 

                if (!string.IsNullOrEmpty(userID))  // Ager Search Term null nahi hai to 
                {
                    orders = orders.Where(x => x.UserID.ToLower().Contains(userID.ToLower())).ToList(); // vo product lelo jiska name product contain krta ho
                }

                if (!string.IsNullOrEmpty(status))  // Ager Search Term null nahi hai to 
                {
                    orders = orders.Where(x => x.Status.ToLower().Contains(status.ToLower())).ToList(); // vo product lelo jiska name product contain krta ho
                }

                return orders.Count(); // ab products ko return krdo pagination lga kr 

            }
        }


        public Order GetOrderById(int id)
        {
            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {

                return context.Orders.Where(x => x.ID == id).Include(x => x.OrderItems).Include("OrderItems.Product").FirstOrDefault();
            }
        }

        public bool UpdateOrderStatus(int iD, string status)
        {
            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {
                var order = context.Orders.Find(iD);
                order.Status = status;
                context.Entry(order).State = EntityState.Modified;
                return context.SaveChanges() > 0;

            }
        }
    }


}

