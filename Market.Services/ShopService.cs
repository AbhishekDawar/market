﻿using Market.DataBase;
using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Services
{
   public class ShopService
    {

        #region Singleton

        private static ShopService instance { get; set; }
        public static ShopService Instance
        {
            get
            {
                if (instance == null) instance = new ShopService();
                return instance;
            }
        }

        private ShopService()
        {
        }
        #endregion



        #region ADDing New Order in Database

        public int SaveOrder(Order order)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                context.Orders.Add(order); // adding Category object to  Category table in code

                return   context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion



    }
}
