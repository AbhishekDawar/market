﻿using Market.DataBase;
using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Market.Services
{
    public class ProductsService // to interact with Database
    {

        #region Singleton
        private static ProductsService instance { get; set; }
        public static ProductsService Instance
        {
            get
            {
                if (instance == null) instance = new ProductsService();
                return instance;
            }
        }

        private ProductsService()
        {
        }
        #endregion

        #region Taking  caregory from database on the base of Primary Key ID
        public Product GetProduct(int ID) // Returning Perticular Product on the Base of Product Id which is asked by Controller 
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Products.Where(p => p.ID == ID).Include(c => c.Category).FirstOrDefault();  // // Fatching  Category on the Base of Id 
            }
        }


        //public Product GetProduct(int ID)

        //Used in Product Controller
        // 1:-    when user want to Edit a Product then we fatch Product details on the bases selected Product by user          [HttpGet] Line :- 106
        // 2:-    when user update Product so first we get Product details from DB then update it by user updated details      [HttpPost] Line :- 123
        // 3:-    when user Click on Product then Product Detail page will open there we show A Perticular Product in Detail 



        #endregion




        #region  Taking  caregory from database on the base of Primary Key IDssssss for Making Bill of CArt 

        public List<Product> GetProducts(List<int> IDs)  // Return List of Product On the Bases of Required ID'S Parameter
        {

            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {
                var r = context.Products                                       // Vo products lo
                    .Where(Product => IDs.Contains(Product.ID))              // jiski Product Id's ..Parameter Id's se match kr rhi h
                    .ToList();                                              // ouski List banakr Return krdo

                return r;
            }
        }

        //public List<Product> GetProducts(List<int> IDs)

        //Used in Show Controller (Checkout Action Method)

        // 1:- Line 78 ..Taking Product Cart IDs From Cookies then set it to  ""  List<Product> GetProducts(List<int> IDs)  "" and in return 
        // take list of products in Shop Controller Checkout Method then show those products in checkout page 



        #endregion

        #region 
        public int GetMaximumPrice()
        {
            using (var context = new MarketContext())  // Making object of DAL layer so that we can Communicate with  database
            {

                int products = context.Products.Count();   // Taking All Product Count

                if (products != 0)  // Ager products hai .. Database me 
                {
                    return (int)(context.Products.Max(product => product.Price));  // To ouski Maximum Price Send Kardo
                }
                else { return 0; }      // If koi product nahi hai to return krdo 
            }
        }


        //public  int GetMaximumPrice()

        //Used in Show Controller 

        // 1:- Line 33 ..Taking Maximum price In ShopController then Assign it into Model.Maxumum Price then sent it to the Shop Index view
        // so that we can sort Products on the base of Maximum and Minimum price by Jqurey 



        #endregion


        #region Taking Total Product count and Product Count on the bases of Search Term

        public int GetProductsCount(string search)  //Search Parameter == Product Which is Searched in product Table by Admin 
        {
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                if (!string.IsNullOrEmpty(search))  // If SearchTerm is not NUll
                {
                    //model.Categories = model.Categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower())).ToList();


                    return context.Products                                                   // Vo Product Lo
                        .Where(product => product.Name != null &&                            // jinka Name null na ho or
                        product.Name.ToLower().Contains(search.ToLower()))                  //Product name Search tearm Contain krta ho 
                        .Count();                                                          // and oun Products ka COUNT bta do 
                }
                else
                {
                    return context.Products.Count(); // Sab products ka Count bta do 
                }

            }

        }
        #endregion



        #region Taking List of Products For Admin with PAGINATION on the base of search and without Search

        public List<Product> GetProducts(string search, int pageNo)   //Returning Product list and taking Search tearm and pageNo for pagination from user
        {
            int pageSize = int.Parse(ConfigurationService.Instance.GetConfiguration("ListingPageSize").Value);  // Taking Page Size by Configuration Service

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                if (!string.IsNullOrEmpty(search)) // Ager Koi Search Term hai to  If section WOrk krega 
                {
                    //model.Categories = model.Categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower())).ToList();

                    return context.Products.Where(product => product.Name != null &&  // Vo products lo jiska Name null na ho 
                             product.Name.ToLower().Contains(search.ToLower()))      //or Product ka name search term Contain krta ho 
                             .OrderBy(product => product.ID)                        // ab jo products hai ousko accending Order me lagao
                             .Skip((pageNo - 1) * pageSize)                        // according to Page number and size product manage kro 
                             .Take(pageSize)                                      // Ab product lelo According to Page size .. like pagesize is 4 so take 4 product
                             .Include(product => product.Category)               // Product me ouski Category bhi include krvalo 
                             .ToList();                                         // finnaly product ki list bnao and bejhdo
                }

                else
                {
                    return context.Products                                 //  products lo
                        .OrderBy(x => x.ID)                                // ab jo products li hai ousko accending Order me lagao
                        .Skip((pageNo - 1) * pageSize)                    // according to Page number and size product manage kro 
                        .Take(pageSize)                                  // Ab product lelo According to Page size .. like pagesize is 4 so take 4 product
                        .Include(x => x.Category)                       // Product me ouski Category bhi include krvalo 
                        .ToList();                                     // finnaly product ki list bnao and bejhdo

                }
            }

        }

        // public List<Product> GetProducts(string search, int pageNo)


        //Used in

        // 1. Product Controler LINE :- 44  Showing All Products To Admin When Admin Click on Product List ( used Category Table as partial view in Admin DashBoard)


        #endregion

        #region


        public List<Product> SearchProducts(string searchTerm, int? minimumPrice, int? maximumPrice, int? CategoryID, int? sortBy, int pageNo, int pageSize)
        {
            using (var context = new MarketContext())   // making object of DAL layer so that we can Communicate with  database
            {
                var products = context.Products.ToList();  // Products ki List Li  hai Database se 

                if (CategoryID.HasValue)      // Ager Category Id me value hai 
                {
                    products = products.Where(x => x.CategoryID == CategoryID.Value).ToList(); // vo products lelo jiski Category Id aahi hai view se 
                }

                if (!string.IsNullOrEmpty(searchTerm))  // Ager Search Term null nahi hai to 
                {
                    products = products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList(); // vo product lelo jiska name product contain krta ho
                }
                if (minimumPrice.HasValue) // ager minimum price me value hai to 
                {
                    products = products.Where(x => x.Price >= minimumPrice.Value).ToList();   // sbsey kam Price vala product lelo
                }
                if (maximumPrice.HasValue) // ager maximum price me value hai to 
                {
                    products = products.Where(x => x.Price <= maximumPrice.Value).ToList(); // sbsey Jyadha  Price vala product lelo
                }
                if (sortBy.HasValue) // ager sortBY  me value hai to 
                {
                    switch (sortBy.Value)   // Switch case ke according niche ki statement run hogi
                    {

                        case 2: //popular products
                            products = products.OrderBy(x => x.ID).ToList();
                             //  product ko ouski ID's ke according descending  me sort krdo
                            break;
                        case 3: //low to high products
                            products = products.OrderBy(x => x.Price).ToList();         // product ko ouske Price ke according me sort krdo
                            break;
                        case 4: //high to low products
                            products = products.OrderByDescending(x => x.Price).ToList();       //  product ko ouske Price ke according descending  me sort krdo
                            break;
                        default: //new products
                            products = products.OrderByDescending(x => x.ID).ToList();     //  product ko ouske Price ke according descending  me sort krdo
                            break;
                    }
                }
                return products.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList(); // ab products ko return krdo pagination lga kr 
            }
        }



        // public List<Product> SearchProducts(string searchTerm, int? minimumPrice, int? maximumPrice, int? CategoryID,int? sortBy,int pageNo,int pageSize)


        //Used in  Shop Controler

        // 1. Line :- 35   public ActionResult Index

        // jab First time Shop Page open hoga to searchTerm, minimumPrice,  maximumPrice, CategoryID, sortBy ki value null hogi and  pageNo = 1, pageSize =9 hoga
        // tab line number 227 diract run hogi Product Service ki or khali Pagination implement hogi and products ki values return ho jahegi ShopController me 
        //public ActionResult Index method ko then products ki values shop chontroller se.. ShopViewModel me copy krke view ko send kr di jahengi 
        //and view me ham sb products ko view krva dehenge

        // 2. Line 58      ( Must Read Above Line 1 for this )
        // abhi hamhne oupr padha..jab First time Shop Page open hoga to ye service call hogi... but in view ham.. ek  FilerViewmodel ka Object banahenge 
        // or ousme ShopView model se Products Copy kr lehenge.. and vahi pr  Html.RenderPartial("FilterProducts", filterProductViewModel); se ouska 
        //partial view le lehenge jisme ham products show krva rhe hai and pagenation kr rhe hai ..and future me products ko filter bhi vali se krhenge

        //or ye partial view only for product easliye banaya hai.. because product dynamic hai and filter static.. taki site jquery se only product menupulate ho
        //and site fast chale pura page na load krna padhe 

        // or jab ham shop page pr koi filter apply krte hai like searchTerm, minimumPrice,  maximumPrice, CategoryID, sortBy then again ye service call hogi
        //and according to product Filtre like searchTerm, minimumPrice,  maximumPrice, CategoryID, sortBy category hamko result mil jahega 



        #endregion



        #region

        // shop page pr..products pr filter apply krne ke phele or badme..ham count find kr rhe hai pagination krne ke liye 

        public int SearchProductsCount(string searchTerm, int? minimumPrice, int? maximumPrice, int? CategoryID, int? sortBy)
        {
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                var products = context.Products.ToList();  // Products ki List Li  hai Database se 

                if (CategoryID.HasValue) // Ager Category Id me value hai 
                {
                    products = products.Where(x => x.CategoryID == CategoryID.Value).ToList(); // vo products lelo jiski Category Id aahi hai view se 
                }

                if (!string.IsNullOrEmpty(searchTerm)) // Ager Search Term null nahi hai to 
                {
                    products = products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();  // vo product lelo jiska name product contain krta ho
                }
                if (minimumPrice.HasValue)  // ager minimum price me value hai to 
                {
                    products = products.Where(x => x.Price >= minimumPrice.Value).ToList();   // sbsey kam Price vala product lelo
                }
                if (maximumPrice.HasValue)  // ager maximum price me value hai to 
                {
                    products = products.Where(x => x.Price <= maximumPrice.Value).ToList();  // sbsey Jyadha  Price vala product lelo
                }
                if (sortBy.HasValue)  // ager sortBY  me value hai to 
                {
                    switch (sortBy.Value)  // Switch case ke according niche ki statement run hogi
                    {

                        case 2: //popular products
                            products = products.OrderByDescending(x => x.ID).ToList();      //  product ko ouski ID's ke according descending  me sort krdo
                            break;
                        case 3: //low to high
                            products = products.OrderBy(x => x.Price).ToList();           // product ko ouske Price ke according me sort krdo
                            break;
                        case 4: //high to low
                            products = products.OrderByDescending(x => x.Price).ToList();        //  product ko ouske Price ke according descending  me sort krdo
                            break;
                        default: //new
                            products = products.OrderBy(x => x.ID).ToList();                  //  product ko ouske Price ke according descending  me sort krdo
                            break;
                    }
                }
                return products.Count(); // ab products ka count return krdo
            }
        }


        #endregion



        #region taking latest products

        public List<Product> GetLatestProducts(int numberofProducts) // taking Parameter for how many product i have to return 
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Products                 // Sab Products lelo Database se
                    .OrderByDescending(x => x.ID)      // ab products jo li h ousko Id ke according Decending me sort krlo
                    .Take(numberofProducts)           // ab ous me se 4 products lelo
                    .Include(x => x.Category)        // ab jo products li h 4 ouski Category Include krvalo
                    .ToList();                      // ab jo 4 products h ouski list bnao and return krvado


                //return context.Products.Include(x=>x.Category).ToList();
                //  return context.Products.Include("Category").ToList();
                //  return context.Products.Include("Category.Products").ToList();

            }
        }


        // public List<Product> GetLatestProducts(int numberofProducts)


        //Used in

        // 1. Widgets Controller  LINE :- 24
        // Taking 4 latest product for Home page latest Product Section


        #endregion



        #region   Taking Product List

        public List<Product> GetProducts(int pageNo, int pageSize)  // getting products 
        {
            //if (pageSize.Equals(null))
            //{
            //}
            int pages = (pageNo - 1) * pageSize;  // calculating how many products have to skip according to pagination 
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Products                         // products lelo
                    .OrderByDescending(x => x.ID)              // order/sort krlo then descending me 
                    .Skip(pages)                              // etne product skip krdo
                    .Take(pageSize)                          // bache products me se etne products lelo
                    .Include(x => x.Category)               // products me category include krvalo
                    .Distinct()                            // product repeate na ho Distinct hone chiye
                    .ToList();                            // list bnao and return krvado 



                //return context.Products.Include(x=>x.Category).ToList();
                //  return context.Products.Include("Category").ToList();
                //  return context.Products.Include("Category.Products").ToList();

            }
        }


        // public List<Product> GetProducts(int pageNo, int pageSize) 


        //Used in

        // 1. Widgets Controller  LINE :- 37
        // Taking 8 latest product for Home page Our Product Section


        #endregion


        #region taking category products by id 

        public List<Product> GetProductsByCategory(int categoryID, int pageSize)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Products.Where(x => x.CategoryID == categoryID).OrderByDescending(x => x.ID).Take(pageSize).Include(x => x.Category).ToList();
                //return context.Products.Include(x=>x.Category).ToList();
                //  return context.Products.Include("Category").ToList();
                //  return context.Products.Include("Category.Products").ToList();

            }
        }
        #endregion


        #region Not Using it 
        public List<Product> GetProducts(int pageNo)
        {
            int pageSize = 5;
            int pages = (pageNo - 1) * pageSize;
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Products.OrderBy(x => x.ID).Skip(pages).Take(pageSize).Include(x => x.Category).ToList();
                //return context.Products.Include(x=>x.Category).ToList();
                //  return context.Products.Include("Category").ToList();
                //  return context.Products.Include("Category.Products").ToList();

            }
        }

        #endregion

        //SAVE----------------UPDATE ---------------DELETE

        #region Save Product in Database
        public void saveProduct(Product product) // taking Values from caregory object to save in database
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                context.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;
                context.Products.Add(product); // adding Category object to  Category table in code
                context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion

        #region Update product in database
        public void updateProduct(Product product)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                //take product from database and update its propertites from the supplied object
                //var productindb = context.Products.Where(x => x.ID == product.ID).FirstOrDefault();
                //productindb.Name = product.Name;

                //productindb.Description = product.Description;

                //productindb.Price = product.Price;
                //if(!string.IsNullOrEmpty(product.ImageUrl))
                //{
                //    productindb.ImageUrl = product.ImageUrl;
                //}


                //productindb.Category = product.Category;


                context.Entry(product).State = System.Data.Entity.EntityState.Modified;

                context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion

        #region Delete product From database
        public void DeleteProduct(int ID)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {

                //  context.Entry(category).State = System.Data.Entity.EntityState.Modified;

                var product = context.Products.Find(ID);
                context.Products.Remove(product);
                context.SaveChanges();  // Now saving Product into database 
            }
        }
        #endregion







    }
}






