﻿using Market.DataBase;
using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Services
{
    public class ConfigurationService
    {

        #region Singleton
        private static ConfigurationService instance { get; set; }
        public static ConfigurationService Instance
        {
            get
            {
                if (instance == null) instance = new ConfigurationService();
                return instance;
            }
        }

        private ConfigurationService()
        {
        }
        #endregion

        //public List<Config> GetConfigurationList(string Key)
        //{
        //    using (var context = new MarketContext())
        //    {
        //        //  return context.Configurations.Find(Key); you can use this also
        //        return context.Configurations.ToList();

        //    }

        //}


        public Config GetConfiguration(string Key)
        {
            using (var context = new MarketContext())
            {
                //  return context.Configurations.Find(Key); you can use this also
                return context.Configurations.Where(c => c.Key == Key).FirstOrDefault();

            }

        }

        public int PagesizeConfiguration()
        { 
            using (var context = new MarketContext())
            {

                var pageSize = context.Configurations.Find("ListingPageSize").Value;

                return pageSize != null ? int.Parse(pageSize) : 10;

            }

        }

        public List<Config> GetConfigurations()
        {
            using (var context = new MarketContext())
            {
                return context.Configurations.ToList();
            }

        }

        public void SaveConfiguration(Config config)
        {
            using (var context = new MarketContext())
            {
                context.Configurations.Add(config);
                context.SaveChanges();
            }
        }

        public void UpdateConfiguration(Config config)
        {
            using (var context = new MarketContext())
            {
                context.Entry(config).State = System.Data.Entity.EntityState.Modified;


                context.SaveChanges();
            }
        }

        public void DeleteConfiguation(string key)
        {
            using (var context = new MarketContext())
            {
                var Config = context.Configurations.Find(key);
                context.Configurations.Remove(Config);
                context.SaveChanges();
            }

        }

        public int GetConfigurationCount( )
        {
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Configurations.Count();
            }

        }
    }
}