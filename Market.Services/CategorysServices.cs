﻿using Market.DataBase;
using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Market.Services
{
    public class CategorysServices // to interact with Database
    {
        #region Singleton

        private static CategorysServices instance { get; set; }
        public static CategorysServices Instance
        {
            get
            {
                if (instance == null) instance = new CategorysServices();
                return instance;
            }
        }

        private CategorysServices()
        {
        }
        #endregion


        #region Takeing category from Database by Primary key (ID)

        public Category GetCategory(int ID)   //Taking Id as Parameter
        {

            using (var context = new MarketContext())   // making MarketContext ( DATABASE) object by this we will interect with databse 
            {
                return context.Categories.Find(ID);   // Fatching  Category on the Base of Id 
            }
        }



        //public Category GetCategory(int ID)

        //Used in

        //Category Controller  :=   when user want to Edit  a Category then we fatch Category details on the bases selected category by user          [HttpGet]
        //Category Controller  :=   when user update category so first we get category details from DB then update it by user updated details         [HttpPost]
        //Product Controller   :=   when we creating new Product so we need category in which we will
        //                          add new  product so there we fatching category and adding product[HttpPost]




        #endregion


        #region Taking Category count on the base of Category SEARCH  OR  ALL Category Count

        public int GetCategoriesCount(string search)   // Return Type is int because we sending number in return
        {
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                if (!string.IsNullOrEmpty(search))    //if section will work if Search parameter contain any value
                {

                    return context.Categories.Where(category => category.Name != null &&
                             category.Name.ToLower().Contains(search.ToLower())).Count();  //taking matching search category Count 
                }
                else
                {
                    return context.Categories.Count(); //taking All category Count 
                }
            }

        }


        // int GetCategoriesCount(string search) 

        //Used in

        // 1. Admin index Page where we are showing Total category Count to Admin  (Admin Controller Index Action Method LINE 16) 

        // 2. Category Controller (LINE 36) when user search any category with partial name so we count all related partial name categories 
        //     and display those categories with Count of it  




        #endregion


        #region Taking List of Categories From DataBase 


        public List<Category> GetAllCategories()   // return type  list because we returning list of products
        {
            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Categories
                        .ToList();                  // Taling all Category from DB
            }
        }


        // List<Category> GetAllCategories()

        //Used in

        // 1. Product Controler LINE :- 71  when creating product at that time we are showing all category in dropdownlist    Create [HTTPGET]

        // 2.  Product Controler LINE :- 114  when Editing product at that time we are showing all category in dropdownlist    Edit [HTTPGET]



        #endregion


        #region Taking List of categories For Admin with PAGINATION on the base of search and without Search


        public List<Category> GetCategories(string search, int pageNo)   //Returning Category list and taking Search tearm and pageNo for pagination from user
        {
            int pageSize = int.Parse(ConfigurationService.Instance.GetConfiguration("ListingPageSize").Value);    //Taking Pagesize from Configuration Service 


            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {

                if (!string.IsNullOrEmpty(search))  // If section work when  Search Tearm is not null or empty 
                {

                    //model.Categories = model.Categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower())).ToList(); 


                    return context.Categories                                                       // vo Categories lao Database se 
                             .Where(category => category.Name != null && category.Name.ToLower()   // jisme Category Name null na ho and Category Name lowercase me krdo
                             .Contains(search.ToLower()))                     // search term ..lowercase me kro then vo Category lo.. vo Search Tearm Contain krta ho ..
                             .OrderBy(category => category.ID)               // Ab categories ko CategoryId ke According Sort kro Accending (by default) 
                             .Skip((pageNo - 1) * pageSize)                 // Category Skip kro according to page no and page size (like Pagesize 4 h to 4 Categories Select kro)
                             .Take(pageSize)                               // Ab page size ke according Category Select kro 
                             .Include(category => category.Products)      // Ab jo category Select kri h ousme ouske Products Include kro
                             .ToList();                                  // Ab jo Categories mili h ouski List bnao and Return krdo 
                }

                else
                {
                    return context.Categories                          // Categories lao Database se 
                        .OrderBy(category => category.ID)                           // Ab categories ko CategoryId ke According Sort kro Accending (by default) 
                        .Skip((pageNo - 1) * pageSize)               // Category Skip kro according to page no and page size   
                        .Take(pageSize)                             // Ab page size ke according Category Select kro (like Pagesize 4 h to 4 Categories Select kro)
                        .Include(category => category.Products)                  // Ab jo category Select kri h ousme ouske Products Include kro
                        .ToList();                                // Ab jo Categories mili h ouski List bnao and Return krdo 

                }
            }

        }


        // public List<Category> GetCategories(string search, int pageNo)

        //Used in

        // 1. Category Controler LINE :- 38  Showing All Categories To Admin When Admin Click on Category List ( used Category Table as partial view in Admin DashBoard)






        #endregion


        #region Taking List of FEATURED Categories From DataBase 


        public List<Category> GetFeaturedCategories()    //Retening Featured Categories
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                return context.Categories                                                // Categories lao Database se 
                    .Where(category => category.IsFeatured && category.ImageUrl != null)  //vo category lo jo featured h and image h jisme
                    .Include(category => category.Products)               // Ab jo category Select kri h ousme ouske Products Include kro
                    .Take(10) // Ab page size ke according Category Select kro (like Pagesize 4 h to 4 Categories Select kro)
                    .ToList();         // Ab jo Categories mili h ouski List bnao and Return krdo 
            }
        }




        // public List<Category> GetFeaturedCategories() 

        //Used in

        // 1. Home Controler LINE :- 18      Taking Feature category for Home page  
        // 2. Shop Controler  LINE :- 31     Taking feature category to shop controller then store it in ShopModelView 
        //..for Shop View..side filter  on base of feature category 
        // 3. Widgets Controler LINE :- 32 Taking feature category to widget controller then store it in ProductWidgetViewMOdel model object 
        // for sorting products on the base of category .. in home Page .Our products section






        #endregion





        //SAVE----------------UPDATE ---------------DELETE

        #region ADDing New Category in Database
        public void saveCategory(Category category)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                context.Categories.Add(category); // adding Category object to  Category table in code

                context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion


        #region Updating Category in DataBase
        public void updateCategory(Category category)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified; // Updating Category in databse 

                context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion


        #region Delete category from Database
        public void DeleteCategory(int ID)
        {

            using (var context = new MarketContext()) // making object of DAL layer so that we can Communicate with  database
            {

                //  context.Entry(category).State = System.Data.Entity.EntityState.Modified;

                var category = context.Categories.Where(x => x.ID == ID).Include(x => x.Products).FirstOrDefault(); ; // getting perticular category from database by id 

                context.Products.RemoveRange(category.Products); //First Deleting Products of this category 

                context.Categories.Remove(category);   // removing category from data base 
                context.SaveChanges();  // Now saving Category into database 
            }
        }
        #endregion

    }
}







