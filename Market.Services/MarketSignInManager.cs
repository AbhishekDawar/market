﻿using Market.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Market.Services
{
    // Configure the application sign-in manager which is used in this application.
    public class MarketSignInManager : SignInManager<MarketUser, string>
    {
        public MarketSignInManager(MarketUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(MarketUser user)
        {
            return user.GenerateUserIdentityAsync((MarketUserManager)UserManager);
        }

        public static MarketSignInManager Create(IdentityFactoryOptions<MarketSignInManager> options, IOwinContext context)
        {
            return new MarketSignInManager(context.GetUserManager<MarketUserManager>(), context.Authentication);
        }
    }
}
