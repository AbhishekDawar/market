﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Entities
{
    public  class Product : BaseEntity
    {
        public int CategoryID { get; set; } // used in product table to get category id and entity framework will link tihs id with caterory object property 

        public virtual Category Category { get; set; } // link product with category class by making category class object 

        [Required]
        [Range(1,1000000)]
        public decimal Price { get; set; }

        public string ImageUrl { get; set; }


    }
}
