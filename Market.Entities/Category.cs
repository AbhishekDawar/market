﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Entities
{
   public class Category : BaseEntity
    {
        public string ImageUrl { get; set; }
        public List<Product> Products { get; set; } // this means :- catrgory withh hold multiple products thats why i have created list of products
        public bool IsFeatured { get; set; }

    }
}
